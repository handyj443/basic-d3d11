#pragma once
#include <DirectXMath.h>

class cube {
  public:
    cube();
    cube(float x, float y, float z, float r);
    void Update();
	DirectX::XMMATRIX GetWorldMat();
  private:
  	float x, y, z, r;
  	DirectX::XMMATRIX mWorld;
};
