#include "cube.h"
#include <d3d11.h>
#include <DirectXMath.h>
using namespace DirectX;

cube::cube(): x(0.0f), y(0.0f), z(0.0f), r(0.0f), mWorld(XMMatrixIdentity()) { }

cube::cube(float x, float y, float z, float r) : x(x), y(y), z(z), r(r) {
	mWorld = XMMatrixRotationY(r) * XMMatrixTranslation(x, y, z);
}

void cube::Update() {
	// Animate the cube by rotating it in the Y axis
	r += 0.001f;
	mWorld = XMMatrixRotationY(r) * XMMatrixTranslation(x, y, z);
	// Update the constant buffer with the latest world matrix

}

XMMATRIX cube::GetWorldMat() {
	return mWorld;
}